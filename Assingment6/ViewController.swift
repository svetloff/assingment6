//
//  ViewController.swift
//  Assingment6
//
//  Created by Светлов Андрей on 9/10/18.
//  Copyright © 2018 svetloff. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var firstNumberTextField: UITextField!
    @IBOutlet weak var secondNumberTextView: UITextView!
    @IBOutlet weak var resultLabel: UILabel!
    var result = 0
    let keyOfResult = "summOfTwoNumbers"
    let keyOfHistory = "historyCalculating"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let resultString = "\(UserDefaults.standard.integer(forKey: keyOfResult))"
        let historyCalculating = UserDefaults.standard.string(forKey: keyOfHistory)
        resultLabel.text = resultString
        secondNumberTextView.text = historyCalculating
        firstNumberTextField.text = String(arc4random()%10)
    }

    @IBAction func summButtonPressed(_ sender: Any) {
        if let firstNumber = Int(firstNumberTextField.text ?? "") , let secondNumber = Int(secondNumberTextView.text ?? ""){
            
            result = firstNumber + secondNumber
            UserDefaults.standard.set(result, forKey: keyOfResult)
            resultLabel.text = "\(result)"
            secondNumberTextView.font = UIFont(descriptor: UIFontDescriptor(), size: CGFloat(result))
            var historyCalculating = UserDefaults.standard.string(forKey: keyOfHistory) ?? ""
            if historyCalculating != "" {
               historyCalculating += "\n"
            }
            historyCalculating += "\(firstNumber) + \(secondNumber) = \(result)"
            UserDefaults.standard.set(historyCalculating, forKey: keyOfHistory)
        }
        else {
            print("Enter correct numbers to TextField and TextView")
        }

    }

}

